<?php
   include('../config/session.php');
   include('../config/connection.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Order</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="../css/bootstrap.min.css" rel="stylesheet" />
  <link href="../css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="../img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="../dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="../product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="../customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li class="active">
		          	<a href="../order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="../user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include '../page/navbar.php' ?>  

		    <!-- dashboard batch view -->
		<div class="content">
	<div class="row">
	 <div class="col-md-12">
	  <div class="card">
	   <div class="card-header">
	   		<div class="d-flex justify-content-between">
	   			<div>
					<h4 class="card-title">Biaya Pengeluaran</h4>
		 			<p class="card-category">Dashboard > Order > Biaya Pengeluaran</p>
		 		</div>
		 		<div class="d-flex align-items-center">
		 			<button class="btn btn-primary btn-block text-uppercase" data-toggle="modal" data-target="#add_epds_modal"><i class="nc-icon nc-simple-add"></i> Input Biaya Pengeluaran</button>
				</div>
			</div>
	   </div>
	  	<div class="card-body">
	 	 <div class="table-responsive">
		  <table class="table">
		   <thead class=" text-primary">
			<th>
			 Keterangan Biaya Keluar
			</th>
			<th>
			 Nominal Pengeluaran
			</th>
			<th>
			 
			</th>
		   </thead>
		  <tbody>
		  	<?php
		        $sql = "SELECT id_bya, ket_bya, no_bya, bya_batch_id FROM byap";
									         
				$result = mysqli_query( $db, $sql );

				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			?>
		  	<tr>				                    
		  	<td>
		  		<?php echo $row['ket_bya'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['no_bya'] ?>
		  	</td>
		  	<td>
		  		<button class="btn btn-success openModal" href="#viewbya" id="modalBya" data-toggle="modal" data-target="#view_bya_modal" data-id="<?php echo $row['id_bya'] ?>">
		  			<i class="nc-icon nc-ruler-pencil"></i>
		  		</button>
		  	</td>
		  </tr>
		  <?php }?>
		  </tbody>
	     </table>
	    </div>
  	   </div>
	  </div>
	 </div>
	</div>
</div>    

<?php include '../footer.php' ?>
	<!-- modal search product -->
			<div class="modal fade" id="add_epds_modal" role="submit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center><span class="modal-title">Input Pengeluaran</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>

			            <div class="modal-body">
						<form class="form-addbatch" method="POST" action="bya.php">
						<div class="form-label-group">
			              	<p>Kode Batch :</p>
			                <input type="text" id="" name="batchno" class="form-control" placeholder="Nomor Batch" required autofocus>
			                <br>
			              </div>
			              <div class="form-label-group">
			              	<p>Keterangan Biaya Pengeluaran :</p>
			                <input type="text" id="" name="ketBya" class="form-control" placeholder="Keterangan Pengeluaran" required autofocus>
			                <br>
			              </div>
			              <div class="form-label-group">
			              	<p>Nominal Pengeluaran :</p>
			                <input type="text" id="" name="nomBya" class="form-control" placeholder="Nominal Pengeluaran" required autofocus>
			                <br>
			              </div>
			              <div class="modal-footer">
							<button class="btn btn-success" name="add_bya" type="submit">Save</button>
						  </div>
			            </form>
					</div>
			</div>
			</div>
		</div>
		</div>

		<div class="modal fade" id="view_bya_modal" role="_GET">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Edit Biaya Pengeluaran</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-addbatch" method="POST" action="bya.php">
			              <div class="form-label-group">
			              	<p>kode Batch :</p>
			                <input type="batchno" id="viewBatchno" name="batchno" class="form-control" placeholder="Nomor Batch" required autofocus disabled="true">
			                <input type="hidden" id="viewIDBya" name="idBya" class="form-control">
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Keterangan Biaya Pengeluaran :</p>
			                <input type="text" id="viewKetBya" name="ketBya" class="form-control" placeholder="Keterangan Biaya Pengeluaran" required >
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nomimal Biaya Pengeluaran :</p>
			                <input type="text" id="viewNomBya" name="nomBya" class="form-control" placeholder="Nomimal Biaya Pengeluaran :" required >
			               <br>
			              </div>
			              <div class="modal-footer">
							<button class="btn btn-success" name="update_bya" type="submit">Save Change</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>


			

	<!--   Core JS Files   -->
  	<script src="../js/core/jquery.min.js"></script>
  	<script src="../js/core/popper.min.js"></script>
  	<script src="../js/core/bootstrap.min.js"></script>
  	<script src="../js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="../js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="../js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="../js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

  	<script>
	  $('.openModal').click(function(){
	      var id = $(this).attr('data-id');

  		  $.ajax({
	         type: "POST",     // We want to use POST when we request our PHP file
	         url : "bya.php",
	         data : { id_byaa : id },    // passing an array to the PHP file with the param,  value you passed, this could just be a single value i.e. data: your_param
	         cache: false,       // disable the cache optional

	         // Success callback if the PHP executed  
	         success: function(data) {
	              // do somethig - i.e. update your modal with the returned data object

	              var result = data.split('~');

	             
	               $('.modal-body #viewIDBya').val(result[0]);
	              $('.modal-body #viewBatchno').val(result[1]);
	              $('.modal-body #viewKetBya').val(result[2]);    
	              $('.modal-body #viewNomBya').val(result[3]);   
	         }

	     });
		});
	</script>


</body>
</html>