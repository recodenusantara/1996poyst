<?php
   include('config/connection.php');
?>
<div class="content" id="dabatch">
		        <div class="row">
		          <div class="col-md-12">
		            <div class="card demo-icons">
		              <div class="card-header">
		                <p class="card-category">Dashboard > Batch</p>
		              </div>

		              <div class="card-body all-icons">
		              	<div id="batch-wrapper">
		              		<section>
		              			<ul>
		              				<li>
		              					<a href="#addbatch" data-toggle="modal" data-target="#add_batch_modal">
		              						<i class="nc-icon nc-simple-add"></i>
		              						<p>Buka batch baru</p>
		              					</a>
		              				</li>

		              				<?php
		              					$sql = "SELECT batchno FROM batch_sale";
									         
									    $result = mysqli_query( $db, $sql );

									    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
							        ?>
									        <li>
				              					<a class="openModal" href="#viewbatch" data-toggle="modal" data-target="#view_batch_modal" data-id="<?php echo $row['batchno'] ?>">
				              						<i class="nc-icon nc-layout-11"></i>
		                        					<p>Batch no <?php echo $row['batchno'] ?></p>
		                        				</a>
				              				</li>
		              				<?php
									    }
		              				?>
		              				<!-- ksh recycle view jadi dia nambah sendiri kalau ada yang baru -->
		              				
		              			</ul>
		              		</section>
		              	</div>
		              </div>
             </div>
            </div> 
		</div>



</div>