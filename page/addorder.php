<?php
   include('../config/session.php');
   include('../config/connection.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="../img/logo.png">
	  <link rel="icon" type="image/png" href="../img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Product</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="../css/bootstrap.min.css" rel="stylesheet" />
  <link href="../css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="../img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="../dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="../product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="../customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li class="active">
		          	<a href="../order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="../user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include '../page/navbar.php' ?>  

		    <!-- dashboard batch view -->
		<div class="content" id="content">
		     <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Buat Orderan Baru</h5>
                <p class="card-category">Dashboard > Order > Add</p>
              </div>
              <div class="card-body">
                <form action="../order_logic.php" method="POST">
                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Kode Batch</label>
                        <input id="kode_batch_hidden" name="kode_batch" type="hidden" class="form-control" value="">
                        <input id="kode_batch" name="kode_batch" type="text" class="form-control pencarian" value="">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Kode induk Customer</label>
                        <input id="id_customer" name="id_customer" type="text" class="form-control pencarianC"  value="">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Kode Product</label>
                        <input id="kode_product_hidden" name="kode_product" type="hidden" class="form-control" value="">
                        <input id="kode_product" name="kode_product" type="text" class="form-control pencarianP"  value="">
                      </div>
                    </div>
                    
                  </div>

                  <div class="row">
                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label>Jumlah Product yang di pesan</label>
                        <input name="qty" type="number" class="form-control" value="">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button name="buat_orderan" type="submit" class="btn btn-primary btn-round">Buat Orderan</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
             	</div>

	<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
           <!-- Modal content-->
           <div class="modal-content">
              <div class="modal-header">
              	<center>  <span class="modal-title">Nomor Batch</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
              </div>
              <div class="modal-body">
                 <table id="example" class="table table-bordered">
                    <thead>
                       <tr>
                          <th>No. Batch</th>
                          <th>Keterangan</th>
                       </tr>
                    </thead>
                    <tbody>
                    	<?php
					        $sql = "SELECT * FROM batch_sale";
												         
							$result = mysqli_query( $db, $sql );

							while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
						echo '<tr id="data" onClick="masuk(this,'.$row['batchno'].')" href="javascript:void(0)">
                          <td><a id="data" onClick="masuk(this,'.$row['batchno'].')" href="javascript:void(0)">'.$row['batchno'].'</a></td>
                          <td>'.$row['details'].'</td>
                       </tr>';
							}
						?>
                    </tbody>
                 </table>
              </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
           </div>
        </div>
     </div>


     <div class="modal fade" id="myModalC" role="dialog">
        <div class="modal-dialog">
           <!-- Modal content-->
           <div class="modal-content">
              <div class="modal-header">
              	<center>  <span class="modal-title">Nomor Induk Customer</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
              </div>
              <div class="modal-body">
                 <table id="exampleC" class="table table-bordered">
                    <thead>
                       <tr>
                       	  <th>ID Customer</th>
                          <th>Nama Customer</th>
                          <th>No Contact</th>
                          <th>Alamat</th>
                       </tr>
                    </thead>
                    <tbody>
                    	<?php
					        $sql = "SELECT * FROM customer";
												         
							$result = mysqli_query( $db, $sql );

							while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
						echo '<tr id="data" onClick="masukC(this,'.$row['id'].')" href="javascript:void(0)">
                          <td><a id="data" onClick="masukC(this,'.$row['id'].')" href="javascript:void(0)">'.$row['full_name'].'</a></td>
                          <td>'.$row['phone1'].'</td>
                          <td>'.$row['address1'].'</td>
                       </tr>';
							}
						?>
                    </tbody>
                 </table>
              </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
           </div>
        </div>
     </div>

     <div class="modal fade" id="myModalP" role="dialog">
        <div class="modal-dialog">
           <!-- Modal content-->
           <div class="modal-content">
              <div class="modal-header">
              	<center>  <span class="modal-title">Kode Product</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
              </div>
              <div class="modal-body">
                 <table id="exampleP" class="table table-bordered">
                    <thead>
                       <tr>
                       	  <th>Kode Produk</th>
                          <th>Nama Produk</th>
                          <th>Harga Jual</th>
                          <th>Keterangan</th>
                       </tr>
                    </thead>
                    <tbody>
                    	<?php
					        $sql = "SELECT * FROM product";
												         
							$result = mysqli_query( $db, $sql );

							while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
						echo '<tr id="data" onClick="masukP(this,\''.$row['code'].'\')" href="javascript:void(0)">
                          <td><a id="data" onClick="masukP(this,'.$row['code'].')" href="javascript:void(0)">'.$row['code'].'</a></td>
                          <td>'.$row['nama_product'].'</td>
                          <td>'.$row['sellprice'].'</td>
                          <td>'.$row['details'].'</td>
                       </tr>';
							}
						?>
                    </tbody>
                 </table>
              </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
           </div>
        </div>
     </div>

    <?php include '../footer.php' ?>

	<!--   Core JS Files   -->
  	<script src="../js/core/jquery.min.js"></script>
  	<script src="../js/core/popper.min.js"></script>
  	<script src="../js/core/bootstrap.min.js"></script>
  	<script src="../js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="../js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="../js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="../js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

</body>
</html>

<script>
    $('.openModalOrder').click(function(){
      var batchno = $(this).attr('data-id');

      $('#kode_batch').val(batchno);
      $('#kode_batch_hidden').val(batchno);

      $('#search_Batch').modal('hide');
	});

	$('.openModalCustomer').click(function(){
      var id = $(this).attr('data-id');

      $.ajax({
	         type: "POST",     // We want to use POST when we request our PHP file
	         url : "../cust.php",
	         data : { getname : id },    // passing an array to the PHP file with the param,  value you passed, this could just be a single value i.e. data: your_param
	         cache: false,       // disable the cache optional

	         // Success callback if the PHP executed  
	         success: function(data) {
	              // do somethig - i.e. update your modal with the returned data object
      			$('#id_customer').val(id);
      			$('#nama_customer').val(data);
		        $('#search_Customer').modal('hide');
	         }

	     });
	});

	$('.openModalProduct').click(function(){
      var kode_product = $(this).attr('data-id');

      $('#kode_product').val(kode_product);
      $('#kode_product_hidden').val(kode_product);

      $('#search_Product').modal('hide');
	});

	$(document).ready(function() {
  //focusin berfungsi ketika cursor berada di dalam textbox modal langsung aktif
  $(".pencarian").focusin(function() {
    $("#myModal").modal('show'); // ini fungsi untuk menampilkan modal
  });
  $('#example').DataTable(); // fungsi ini untuk memanggil datatable
});

$(document).ready(function() {
  //focusin berfungsi ketika cursor berada di dalam textbox modal langsung aktif
  $(".pencarianC").focusin(function() {
    $("#myModalC").modal('show'); // ini fungsi untuk menampilkan modal
  });
  $('#exampleC').DataTable(); // fungsi ini untuk memanggil datatable
});

$(document).ready(function() {
  //focusin berfungsi ketika cursor berada di dalam textbox modal langsung aktif
  $(".pencarianP").focusin(function() {
    $("#myModalP").modal('show'); // ini fungsi untuk menampilkan modal
  });
  $('#exampleP').DataTable(); // fungsi ini untuk memanggil datatable
});

// function in berfungsi untuk memindahkan data kolom yang di klik menuju text box
function masuk(txt, data) {
  document.getElementById('kode_batch').value = data; // ini berfungsi mengisi value  yang ber id textbox
  $("#myModal").modal('hide'); // ini berfungsi untuk menyembunyikan modal
}

function masukC(txt, data) {
  document.getElementById('id_customer').value = data; // ini berfungsi mengisi value  yang ber id textbox
  $("#myModalC").modal('hide'); // ini berfungsi untuk menyembunyikan modal
}

function masukP(txt, data) {
  document.getElementById('kode_product').value = data; // ini berfungsi mengisi value  yang ber id textbox``
  $("#myModalP").modal('hide'); // ini berfungsi untuk menyembunyikan modal
}
	
</script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>