<?php
   session_start();
   
   $login_session = $_SESSION['login_user'];
   $login_id = $_SESSION['login_id'];

   if(!isset($_SESSION['login_user'])){
      header("location:login.php");
      die();
   }
?>