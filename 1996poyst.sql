-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2019 at 05:16 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1996poyst`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch_sale`
--

CREATE TABLE `batch_sale` (
  `id` int(8) NOT NULL,
  `batchno` int(12) NOT NULL,
  `openpodate` date NOT NULL,
  `closepodate` date NOT NULL,
  `details` varchar(500) NOT NULL,
  `tgltutupbuku` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `payment_order_detail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `batch_sale`
--

INSERT INTO `batch_sale` (`id`, `batchno`, `openpodate`, `closepodate`, `details`, `tgltutupbuku`, `user_id`, `payment_id`, `payment_order_detail`) VALUES
(1, 1, '2019-11-06', '2019-11-20', 'aaa', '0000-00-00', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `byap`
--

CREATE TABLE `byap` (
  `id_bya` int(11) NOT NULL,
  `ket_bya` varchar(30) NOT NULL,
  `no_bya` int(11) NOT NULL,
  `bya_batch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `byap`
--

INSERT INTO `byap` (`id_bya`, `ket_bya`, `no_bya`, `bya_batch_id`) VALUES
(1, 'ticket ', 1100000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `full_name` varchar(225) NOT NULL,
  `phone1` bigint(12) NOT NULL,
  `phone2` bigint(12) NOT NULL,
  `address1` varchar(225) NOT NULL,
  `address2` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `full_name`, `phone1`, `phone2`, `address1`, `address2`) VALUES
(6, 'Rikko S', 85829476319, 81378162878, 'Perum.Sukajadi Jln Kelapa Gading Blok F no 11a', 'Perum.Centre Park Blok A no 9');

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `order_detail_id` int(11) NOT NULL,
  `order_product_customer_id` int(11) NOT NULL,
  `order_product_product_code` varchar(45) NOT NULL,
  `order_product_product_batch_sale_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`order_detail_id`, `order_product_customer_id`, `order_product_product_code`, `order_product_product_batch_sale_id`) VALUES
(8, 7, 'a003', 1),
(9, 7, 'a001', 1),
(10, 7, 'a004', 1),
(11, 6, 'a002', 1),
(12, 6, 'a002', 1),
(13, 7, 'a002', 1),
(14, 6, 'a003', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `customer_id` int(11) NOT NULL,
  `product_code` varchar(45) NOT NULL,
  `product_batch_sale_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `status` varchar(12) DEFAULT NULL,
  `sts_pby` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`customer_id`, `product_code`, `product_batch_sale_id`, `qty`, `status`, `sts_pby`) VALUES
(7, 'a003', 1, 13, 'invoice_prin', 'Lunas'),
(7, 'a001', 1, 13, 'invoice_prin', 'Lunas'),
(7, 'a004', 1, 4, 'invoice_prin', 'Lunas'),
(6, 'a002', 1, 12, 'invoice_prin', 'Lunas'),
(6, 'a002', 1, 12, 'invoice_prin', 'Lunas'),
(6, 'a003', 1, 2, 'invoice_prin', 'Lunas'),
(7, 'a002', 1, 1, 'invoice_prin', 'Lunas');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `detail` enum('Lunas','Pending') NOT NULL,
  `buktibayar` blob NOT NULL,
  `batch_sale_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `invoice_id`, `detail`, `buktibayar`, `batch_sale_id`) VALUES
(3, 11, 'Lunas', '', 1),
(4, 8, 'Lunas', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `code` varchar(16) NOT NULL,
  `nama_product` varchar(225) NOT NULL,
  `buyprice` bigint(20) NOT NULL,
  `sellprice` bigint(20) NOT NULL,
  `vendor` varchar(225) NOT NULL,
  `details` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `code`, `nama_product`, `buyprice`, `sellprice`, `vendor`, `details`) VALUES
(1, 'a001', 'testaz', 12000, 1222, 'vendor', 'keterangan'),
(2, 'a002', 'test1aa', 1234, 122222, 'aab', 'abcd'),
(3, 'a003', 'test2', 123, 122, 'abb', 'abcde');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(6) NOT NULL,
  `username` text NOT NULL,
  `nama_lengkap` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `password` varchar(35) NOT NULL,
  `create_date` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `office_code` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `nama_lengkap`, `email`, `password`, `create_date`, `office_code`) VALUES
(1, 'dev', 'developer', 'developer@email.com', '227edf7c86c02a44d17eec9aa5b30cd1', '2019-11-06 08:00:00.000000', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batch_sale`
--
ALTER TABLE `batch_sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `byap`
--
ALTER TABLE `byap`
  ADD PRIMARY KEY (`id_bya`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`order_detail_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batch_sale`
--
ALTER TABLE `batch_sale`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `byap`
--
ALTER TABLE `byap`
  MODIFY `id_bya` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
