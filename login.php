<?php
   include("config/connection.php");
   session_start();

   if(isset($_SESSION['login_user'])){
      header("location:index.php");
      die();
   }
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($db,$_POST['username']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']); 
      
      $sql = "SELECT id FROM user WHERE username = '$myusername' and password = md5('$mypassword')";
      $result = mysqli_query($db,$sql);
      
      $count = mysqli_num_rows($result);
      
      $id = mysqli_fetch_assoc($result);

      // If result matched $myusername and $mypassword, table row must be 1 row
    
      if($count == 1) {
         $_SESSION['login_user'] = $myusername;
         $_SESSION['login_id'] = $id['id'];
         
         header("location: dashboard.php");
      }else {
        $error = "Your Username or Password is invalid";
        echo "<script type='text/javascript'>alert('$error');</script>";
      }
   }
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996Poyst.css?v=2.0.0" rel="stylesheet" />

</head>
<body class="main" style="background-color: #f4f3ef">
	<div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
        	<div class="row justify-content-md-center">
        		<img style="max-width: 150px; max-height: 150px" src="img/logo.png">
        	</div>
          <div class="card-body">
            <form class="form-signin" method="POST">
              <div class="form-label-group">
                <input type="Username" id="inputUser" name="username" class="form-control" placeholder="Username" required autofocus>
              </div>
              <div class="form-label-group">
              	<br>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
               <br>
              </div>
              <button class="btn btn-lg btn-danger btn-block text-uppercase" type="submit">LOGIN</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>