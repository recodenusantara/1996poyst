<?php
   include('config/connection.php');
?>
<div class="content">
	<div class="row">
	 <div class="col-md-12">
	  <div class="card">
	   <div class="card-header">
	   		<div class="d-flex justify-content-between">
	   			<div>
					<h4 class="card-title"> Daftar Customer</h4>
		 			<p class="card-category">Dashboard > Customer</p>
		 		</div>
		 		<div class="d-flex align-items-center">
		 			<button class="btn btn-primary btn-block text-uppercase" data-toggle="modal" data-target="#add_customer_modal"><i class="nc-icon nc-simple-add"></i> Tambah Customer Baru</button>
				</div>
			</div>
	   </div>
	  	<div class="card-body">
	 	 <div class="table-responsive">
		  <table class="table">
		   <thead class=" text-primary">
			<th>
			 Nama Customer
			</th>
			<th>
			 Alamat 
			</th>
			<th>
 			 No.Contact
			</th>
			<th>

			</th>
		   </thead>
		  <tbody>
		  	<?php
		        $sql = "SELECT * FROM customer";
									         
				$result = mysqli_query( $db, $sql );

				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			?>
		  	<tr>		                    
		  	<td>
		  		<?php echo $row['full_name'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['address1'] ?>
		  	</td>
		  	<td>
		  		(+62) <?php echo $row['phone1'] ?>
		  	</td>
		  	<td>
		  		<button class="btn btn-success openModal" data-toggle="modal" data-target="#view_customer_modal" data-id="<?php echo $row['id'] ?>">
		  			<i class="nc-icon nc-ruler-pencil"></i>
		  		</button>
		  	</td>
		  	</tr>
		  	<?php
				}
			?>
		  </tbody>
	     </table>
	    </div>
  	   </div>
	  </div>
	 </div>
	</div>
</div>     	