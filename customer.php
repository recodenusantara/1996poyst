<?php
   include('config/session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Product</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li class="active">
		          	<a href="#">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li>
		          	<a href="order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		          <li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include 'page/navbar.php' ?>  

		    <!-- dashboard customer view -->
		    <?php include 'page/dacust.php' ?>

		    <?php include 'footer.php' ?>

		    <!-- modal add -->
			<div class="modal fade" id="add_customer_modal" role="submit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Tambah Customer Baru</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-addcust" method="POST" action="cust.php">
			              <div class="form-label-group">
			              	<p>Nama Lengkap Customer :</p>
			                <input type="text" id="inputfull_name" name="Full_name" class="form-control" placeholder="Nama Customer" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Alamat 1 Customer :</p>
			                <input type="text" id="inputaddress1" name="Address1" class="form-control" placeholder="Alamat Customer" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Alamat 2 Customer :</p>
			                <input type="text" id="inputaddress2" name="Address2" class="form-control" placeholder="Alamat Customer">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nomor Contact 1 :</p>
			                <input type="number" id="inputphone1" name="Phone1" class="form-control" placeholder="Nomor Contact Customer">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nomor Contact 2 :</p>
			                <input type="number" id="inputphone2" name="Phone2" class="form-control" placeholder="Nomor Contact Customer">
			               <br>
			              </div>
			              <div class="modal-footer">
							<button name="add_customer" class="btn btn-success" type="submit">SUBMIT</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>

			<!-- modal display cust -->
			<div class="modal fade" id="view_customer_modal" role="_GET">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Edit Customer</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-addcust" method="POST" action="cust.php">
			              <div class="form-label-group">
			              	<p>Nama lengkap customer :</p>
			                <input type="text" id="viewFull_name" name="Full_name" class="form-control" placeholder="Nama Customer" required autofocus>
			                <input type="hidden" id="viewIdHidden" name="id" class="form-control">
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Alamat customer 1 :</p>
			                <input type="text" id="viewAddress1" name="Address1" class="form-control" placeholder="Alamat customer" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Alamat customer 2 :</p>
			                <input type="text" id="viewAddress2" name="Address2" class="form-control" placeholder="Alamat customer" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Contact customer 1 :</p>
			                <input type="number" id="viewPhone1" name="Phone1" class="form-control" placeholder="08xxxxxxxxxx" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Contact customer 2 :</p>
			                <input type="number" id="viewPhone2" name="Phone2" class="form-control" placeholder="08xxxxxxxxxx" required>
			               <br>
			              </div>
			              <div class="modal-footer">
							<button class="btn btn-success" name="update_details" type="submit">Save Change</button>
							<button class="btn btn-danger" name="delete_cust" type="submit">Delete</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>


	<!--   Core JS Files   -->
  	<script src="js/core/jquery.min.js"></script>
  	<script src="js/core/popper.min.js"></script>
  	<script src="js/core/bootstrap.min.js"></script>
  	<script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

  	<script>
	  $('.openModal').click(function(){
	      var id = $(this).attr('data-id');

  		  $.ajax({
	         type: "POST",     // We want to use POST when we request our PHP file
	         url : "cust.php",
	         data : { id_cust : id },    // passing an array to the PHP file with the param,  value you passed, this could just be a single value i.e. data: your_param
	         cache: false,       // disable the cache optional

	         // Success callback if the PHP executed  
	         success: function(data) {
	              // do somethig - i.e. update your modal with the returned data object

	              var result = data.split('~');

	              $('.modal-body #viewIdHidden').val(result[0]);
	              $('.modal-body #viewFull_name').val(result[1]);
	              $('.modal-body #viewAddress1').val(result[2]);
	              $('.modal-body #viewAddress2').val(result[3]);    
	              $('.modal-body #viewPhone1').val(result[4]);    
	              $('.modal-body #viewPhone2').val(result[5]);     
	         }

	     });
		});
	</script>
</body>
</html>