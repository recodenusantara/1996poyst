<?php
   include('config/session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Product</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li class="active">
		          	<a href="#">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		          <li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include 'page/navbar.php' ?>  

		    <!-- dashboard batch view -->
		   	<?php include 'page/ordr.php' ?>



		 <div class="modal fade" id="print_inv_modal" role="submit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Print Invoice</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<table id="example" class="table table-bordered">
                    <thead>
                       <tr>

                          <th>Nama Customer</th>
                       </tr>
                    </thead>
                    <tbody>
                    	<?php
					        $sql = "SELECT id, full_name FROM customer";
												         
							$result = mysqli_query( $db, $sql );

							while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
						?>
						 <tr id="data">
                          <td><a id="data" target="_blank" href="inv.php?customer=<?php echo $row['id']; ?>"><?php echo $row['full_name'] ?> </a></td>
                       </tr>
                       <?php
							}
						?>
                    </tbody>
                 </table>
					</div>
				</div>
			</div>
			</div>
	<!--   Core JS Files   -->
  	<script src="js/core/jquery.min.js"></script>
  	<script src="js/core/popper.min.js"></script>
  	<script src="js/core/bootstrap.min.js"></script>
  	<script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
  //focusin berfungsi ketika cursor berada di dalam textbox modal langsung aktif
  $(".pencarian").focusin(function() {
    $("#myModal").modal('show'); // ini fungsi untuk menampilkan modal
  });
  $('#example').DataTable(); // fungsi ini untuk memanggil datatable
});

// function in berfungsi untuk memindahkan data kolom yang di klik menuju text box
function masuk(txt, data) {
  document.getElementById('kode_batch').value = data; // ini berfungsi mengisi value  yang ber id textbox
  $("#myModal").modal('hide'); // ini berfungsi untuk menyembunyikan modal
}
	
</script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>