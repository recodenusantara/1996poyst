<?php
   include('config/session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Product</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li >
		          	<a href="order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li class="active">
		          	<a href="#">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include 'page/navbar.php' ?>  

		    <!-- dashboard batch view -->
		    <?php include 'page/dauser.php' ?>

		    <?php include 'footer.php' ?>

		    <!-- modal add -->
			<div class="modal fade" id="add_user_modal" role="submit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Tambah User Baru</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-adduser" method="POST" action="userb.php">
			              <div class="form-label-group">
			              	<p>Username :</p>
			                <input type="text" id="userName" name="userName" class="form-control" placeholder="username" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nama Lengkap :</p>
			                <input type="text" id="NamaLengkap" name="NamaLengkap" class="form-control" placeholder="Nama" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Email :</p>
			                <input type="Email" id="Email" name="Email" class="form-control" placeholder="sys@sys.com" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Password :</p>
			                <input type="password" id="Password" name="Password" class="form-control" placeholder="password">
			               <br>
			              </div>
			              <div class="modal-footer">
							<button name="add_user" class="btn btn-success" type="submit">SUBMIT</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>

			<!-- modal display batch -->
			<div class="modal fade" id="view_user_modal" role="_GET">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Detail User</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-uptuser" method="POST" action="userb.php">
			              <div class="form-label-group">
			              	<p>Username :</p>
			                <input type="text" id="VuserName" name="userName" class="form-control" placeholder="username" required autofocus disabled="">
			                <input type="hidden" id="viewIDhidden" name="Vid" class="form-control" placeholder="username" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nama Lengkap :</p>
			                <input type="text" id="VNamaLengkap" name="NamaLengkap" class="form-control" placeholder="Nama" required autofocus disabled="">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Email :</p>
			                <input type="Email" id="VEmail" name="Email" class="form-control" placeholder="sys@sys.com" required autofocus disabled="">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Password :</p>
			                <input type="password" id="VPassword" name="Password" class="form-control" placeholder="password">
			               <br>
			              </div>
			              <div class="modal-footer">
							<button name="upt-user" class="btn btn-success" type="submit">SAVE</button>
							<button name="del-user" class="btn btn-danger" type="submit">DELETE</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>


	<!--   Core JS Files   -->
  	<script src="js/core/jquery.min.js"></script>
  	<script src="js/core/popper.min.js"></script>
  	<script src="js/core/bootstrap.min.js"></script>
  	<script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

  	<script>
	  $('.openModal').click(function(){
	      var id = $(this).attr('data-id');

  		  $.ajax({
	         type: "POST",     // We want to use POST when we request our PHP file
	         url : "userb.php",
	         data : { id_user : id },    // passing an array to the PHP file with the param,  value you passed, this could just be a single value i.e. data: your_param
	         cache: false,       // disable the cache optional

	         // Success callback if the PHP executed  
	         success: function(data) {
	              // do somethig - i.e. update your modal with the returned data object

	              var result = data.split('~');

	              $('.modal-body #viewIDhidden').val(result[0]);
	              $('.modal-body #VuserName').val(result[1]);
	              $('.modal-body #VNamaLengkap').val(result[2]);    
	              $('.modal-body #VEmail').val(result[3]);    
	              $('.modal-body #VPassword').val(result[4]); 
	         }

	     });
		});
	</script>
</body>
</html>