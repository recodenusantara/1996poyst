<?php
   include('config/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996poyst.css?v=2.0.0" rel="stylesheet" />

</head>
<body class="main" style="background-color: #f4f3ef">
	<div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
        	<div class="row d-flex justify-content-center">
        		<img style="max-width: 150px; max-height: 150px" src="img/logo.png">

            <h4>OPPS! Something went wrong</h4> 
            <h2><a href = "logout.php">Sign Out</a></h2>

        	</div>        
        </div>
      </div>
    </div>
  </div>
</body>
</html>