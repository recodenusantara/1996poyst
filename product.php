<?php
   include('config/session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Product</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li class="active">
		          	<a href="#">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li>
		          	<a href="order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		          <li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include 'page/navbar.php' ?>  

		    <!-- dashboard batch view -->
		   	<?php include 'page/daprod.php' ?>

		   	<?php include 'footer.php' ?>

		    <!-- modal add -->
			<div class="modal fade" id="add_product_modal" role="submit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Tambah Product Baru</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-addproduct" method="POST" action="prdct.php">
			              <div class="form-label-group">
			              	<p>Kode Product :</p>
			                <input type="codeProduct" id="inputCodeProduct" name="Code" class="form-control" placeholder="Kode Product" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nama Product :</p>
			                <input type="namaProduct" id="inputNamaProduct" name="Nama_Product" class="form-control" placeholder="Nama Product" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Harga Modal :</p>
			                <input type="buyPrice" id="inputBuyPrice" name="Buyprice" class="form-control" placeholder="Harga Modal" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Harga Jual :</p>
			                <input type="sellPrice" id="inputSellPrice" name="Sellprice" class="form-control" placeholder="Harga Jual" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nama Vendor :</p>
			                <input type="vendor" id="inputVendor" name="Vendor" class="form-control" placeholder="Nama Vendor" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Keterangan :</p>
			                <input type="productdetails" id="inputDetailsProduct" name="Details" class="form-control" placeholder="Keterangan">
			              </div>
			              <div class="modal-footer">
							<button name="add_product" class="btn btn-success" type="submit">SUBMIT</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>

			<!-- modal display product -->
			<div class="modal fade" id="view_product_modal" role="_GET">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Edit Product</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-addproduct" method="POST" action="prdct.php">
			              <div class="form-label-group">
			              	<p>Kode Product :</p>
			                <input type="code" id="viewcode" name="Code" class="form-control" placeholder="Kode Product" required autofocus disabled="true">
			                <input type="hidden" id="viewIdHidden" name="Code" class="form-control">
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nama Product :</p>
			                <input type="nama_product" id="viewNama_Product" name="Nama_Product" class="form-control" placeholder="Nama Product" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Harga Modal :</p>
			                <input type="buyprice" id="viewbuyPrice" name="Buyprice" class="form-control" placeholder="Harga Modal" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Harga Jual :</p>
			                <input type="sellprice" id="viewsellPrice" name="Sellprice" class="form-control" placeholder="Harga Jual" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Nama Vendor :</p>
			                <input type="nama vendor" id="viewvendor" name="Vendor" class="form-control" placeholder="Nama Vendor" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Keterangan :</p>
			                <input type="details" id="viewdetails" name="Details" class="form-control" placeholder="Keterangan">
			               <br>
			              </div>
			              <div class="modal-footer">
							<button class="btn btn-success" name="update_product" type="submit">Save Change</button>
							<button class="btn btn-danger" name="delete_product">Delete</button>
							</div>
			            </form>
					</div>
				</div>
			</div>
			</div>


	<!--   Core JS Files   -->
  	<script src="js/core/jquery.min.js"></script>
  	<script src="js/core/popper.min.js"></script>
  	<script src="js/core/bootstrap.min.js"></script>
  	<script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

  	<script>
	  $('.openModal').click(function(){
	      var id = $(this).attr('data-id');

  		  $.ajax({
	         type: "POST",     // We want to use POST when we request our PHP file
	         url : "prdct.php",
	         data : { code : id },    // passing an array to the PHP file with the param,  value you passed, this could just be a single value i.e. data: your_param
	         cache: false,       // disable the cache optional

	         // Success callback if the PHP executed  
	         success: function(data) {
	              // do somethig - i.e. update your modal with the returned data object

	              var result = data.split('~');

	              $('.modal-body #viewcode').val(result[0]);
	              $('.modal-body #viewIdHidden').val(result[0]);
	              $('.modal-body #viewNama_Product').val(result[1]);    
	              $('.modal-body #viewbuyPrice').val(result[2]);    
	              $('.modal-body #viewsellPrice').val(result[3]);    
	              $('.modal-body #viewvendor').val(result[4]);    
	              $('.modal-body #viewdetails').val(result[5]);    
	         }

	     });
		});
	</script>
</body>
</html>