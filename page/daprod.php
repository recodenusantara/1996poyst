<?php
   include('config/connection.php');
?>
<div class="content">
	<div class="row">
	 <div class="col-md-12">
	  <div class="card">
	   <div class="card-header">
	   		<div class="d-flex justify-content-between">
	   			<div>
					<h4 class="card-title"> Daftar Product</h4>
		 			<p class="card-category">Dashboard > Product</p>
		 		</div>
		 		<div class="d-flex align-items-center">
		 			<button class="btn btn-primary btn-block text-uppercase" data-toggle="modal" data-target="#add_product_modal"><i class="nc-icon nc-simple-add"></i> Tambah Product Baru</button>
				</div>
			</div>
	   </div>
	  	<div class="card-body">
	 	 <div class="table-responsive">
		  <table class="table">
		   <thead class=" text-primary">
			<th>
			 Kode Produk
			</th>
			<th>
			 Nama Produk
			</th>
			<th>
 			 Harga Modal
			</th>
			<th>
 			 Harga Jual
			</th>
			<th>
 			 Vendor
			</th>
			<th>
			 Details
			</th>
			<th>
			 
			</th>
		   </thead>
		  <tbody>
		  	<?php
		        $sql = "SELECT code, nama_product, buyprice, sellprice, vendor, details FROM product";
									         
				$result = mysqli_query( $db, $sql );

				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			?>
		  	<tr>				                    
		  	<td>
		  		<?php echo $row['code'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['nama_product'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['buyprice'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['sellprice'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['vendor'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['details'] ?>
		  	</td>
		  	<td>
		  		<button class="btn btn-success openModal" href="#viewproduct" id="modalProduct" data-toggle="modal" data-target="#view_product_modal" data-id="<?php echo $row['code'] ?>">
		  			<i class="nc-icon nc-ruler-pencil"></i>
		  		</button>
		  	</td>
		  	</tr>
		  	<?php
				}
			?>
		  </tbody>
	     </table>
	    </div>
  	   </div>
	  </div>
	 </div>
	</div>
</div>    		        	