<?php
   include('config/session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Dashboard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li class="active">
		            <a href="#">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li>
		          	<a href="order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include 'page/navbar.php' ?>  

		    <!-- dashboard batch view -->
		    <?php include 'page/dabatch.php' ?>

		    <?php include 'footer.php' ?>

		    <!-- modal add -->
			<div class="modal fade" id="add_batch_modal" role="submit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Tambah Batch Baru</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-addbatch" method="POST" action="batch.php">
			              <div class="form-label-group">
			              	<p>Nomor Batch :</p>
			                <input type="batchno" id="inputBatchno" name="Batchno" class="form-control" placeholder="Nomor Batch" required autofocus>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Tanggal Buka PO :</p>
			                <input type="date" id="inputOpenPOdate" name="openPOdate" class="form-control" placeholder="Today" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Tanggal Tutup PO :</p>
			                <input type="date" id="inputClosePOdate" name="closePOdate" class="form-control" placeholder="Today" required>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Keterangan :</p>
			                <input type="batchdetails" id="inputDetailsBatch" name="details" class="form-control" placeholder="Keterangan">
			               <br>
			               <input type="hidden" id="id" name="id" value="<?php echo $login_id ?>">
			              </div>
			              <div class="modal-footer">
							<button name="add_batch" class="btn btn-success" type="submit">SUBMIT</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>

			<?php 

				foreach ($db ->query('SELECT customer_id, COUNT(*) FROM order_product WHERE status IS NOT NULL;') as $row) {
					$tt = $row['COUNT(*)'];
				}

				foreach ($db ->query('SELECT customer_id, COUNT(*) FROM order_product WHERE sts_pby IS NOT NULL;') as $row2) {
					$ee = $row2['COUNT(*)'];
				}

			 ?>

			<!-- modal display batch -->
			<div class="modal fade" id="view_batch_modal" role="_GET">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<center>  <span class="modal-title">Detail Batch</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<form class="form-addbatch" method="POST" action="batch.php">
			              <div class="form-label-group">
			              	<p>Nomor Batch :</p>
			                <input type="batchno" id="viewBatchno" name="Batchno" class="form-control" placeholder="Nomor Batch" required autofocus disabled="true">
			                <input type="hidden" id="viewBatchnoHidden" name="Batchno" class="form-control">
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Tanggal Buka PO :</p>
			                <input type="date" id="viewOpenPOdate" name="openPOdate" class="form-control" placeholder="Today" required disabled="true">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Tanggal Tutup PO :</p>
			                <input type="date" id="viewClosePOdate" name="closePOdate" class="form-control" placeholder="Today" required disabled="true">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Tanggal Closing :</p>
			                <input type="date" id="inputClosing" name="tgltutupbuku" class="form-control" placeholder="Today" required <?php if ($tt != $ee) { ?> disabled="true" <?php }?>>
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Total Orderan :</p>
			                <input type="total orderan" id="viewTotalOrderan" name="payment_order_detail" class="form-control" value="<?php echo $tt ?>"disabled="true">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Total Orderan Lunas:</p>
			                <input type="orderanlunas" id="viewTotalOrderanLunas" name="payment_id" class="form-control" placeholder="orderan lunas" value="<?php echo $ee ?>" disabled="true">
			               <br>
			              </div>
			              <div class="form-label-group">
			              	<br>
			              	<p>Keterangan :</p>
			                <input type="batchdetails" id="inputDetailsBatch" name="details" class="form-control" placeholder="Keterangan">
			               <br>
			               <input type="hidden" id="id" name="id" value="<?php echo $login_id ?>">
			              </div>
			              <div class="modal-footer">
							<button class="btn btn-success" name="update_details" type="submit">Save Change</button>
							<button class="btn btn-danger" id="btn-cs" type="submit" name="update_details2" <?php if ($tt != $ee) { ?> disabled="true" <?php }?> >Closing dan export laporan</button>
						  </div>
			            </form>
					</div>
				</div>
			</div>
			</div>




	<!--   Core JS Files   -->
  	<script src="js/core/jquery.min.js"></script>
  	<script src="js/core/popper.min.js"></script>
  	<script src="js/core/bootstrap.min.js"></script>
  	<script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

  	<script>
	  $('.openModal').click(function(){
	      var id = $(this).attr('data-id');

  		  $.ajax({
	         type: "POST",     // We want to use POST when we request our PHP file
	         url : "batch.php",
	         data : { batchno : id },    // passing an array to the PHP file with the param,  value you passed, this could just be a single value i.e. data: your_param
	         cache: false,       // disable the cache optional

	         // Success callback if the PHP executed  
	         success: function(data) {
	              // do somethig - i.e. update your modal with the returned data object

	              var result = data.split('~');

	              $('.modal-body #viewBatchno').val(result[0]);
	              $('.modal-body #viewBatchnoHidden').val(result[0]);
	              $('.modal-body #viewOpenPOdate').val(result[1]);    
	              $('.modal-body #viewClosePOdate').val(result[2]);    
	              $('.modal-body #inputDetailsBatch').val(result[3]);    
	              $('.modal-body #inputClosing').val(result[4]);    
	         }

	     });
		});
	</script>
</body>
</html>