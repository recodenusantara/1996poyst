<?php
    include('config/session.php');
    include('config/connection.php');
    
    if(isset($_GET["customer"]))
    {
        $id = $_GET["customer"];

        $sql = "SELECT * FROM order_product WHERE customer_id = $id AND status IS NULL";
												         
		$result = mysqli_query( $db, $sql );

		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			$innersql = "INSERT INTO order_detail VALUES('', '" . $row['customer_id'] . "', '" . $row['product_code'] . "', '" . $row['product_batch_sale_id']. "')";
												         
			$innerresult = mysqli_query( $db, $innersql );

			if(! $innerresult ) {
		         die('Could not enter data: ' . mysqli_error($db));
		      }
		}

		$innersql2 = "UPDATE order_product SET status = 'invoice_printed' WHERE customer_id = '" . $id . "'";
												         
		$innerresult2 = mysqli_query( $db, $innersql2 );

		if(! $innerresult2 ) {
	         die('Could not enter data: ' . mysqli_error($db));
	      }
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body>
	
	<div class="content">
	<div class="row">
	 <div class="col-md-6">
	  <div class="card">
	   <div class="card-header">
	   		<div class="d-flex justify-content-center">
	   			<div>
					<h4 class="card-title">1996 Poyst</h4>
		 		</div>
			</div>
	   </div>
	  	<div class="card-body">
	 	 <div class="table">
	 	 <div class="col-md-12 d-flex justify-content-center">
	 	 	<div class="row">
	 	 		<div class="col-md-6">
	 	 			<?php
	 	 			$sql3 = "SELECT order_detail_id, order_product_customer_id, full_name, order_product_product_code, order_product_product_batch_sale_id, SUM(qty) 'qty', sellprice FROM order_detail INNER JOIN customer ON order_product_customer_id = id INNER JOIN order_product ON order_product_customer_id = customer_id AND order_product_product_code = product_code AND order_product_product_batch_sale_id = product_batch_sale_id INNER JOIN product ON order_product_product_code = code WHERE order_product_customer_id=$id GROUP BY order_product_product_code LIMIT 1";
			         
					$result3 = mysqli_query( $db, $sql3 );

					while($row3 = mysqli_fetch_array($result3, MYSQLI_ASSOC)) {
					
	 	 	 ?>
	 	 			<label>No invoice : <label id="noinv"><?php echo $row3["order_detail_id"];?></label></label> 

	 	 		</div>
	 	 		<div class="col-md-6">
	 	 
	 	 			<label>Nama Customer  <label id=""><?php echo $row3["full_name"]?></label></label>
	 	 		</div>
	 	 	</div>

		  <?php } ?>

	 	 </div>  
	 	 
	 	 <div class=" d-flex justify-content-center">
		  <table class="table-bordered">
		   <thead>
			<th>Kode Produk</th>
			<th>Qty</th>
			<th>Harga/pcs</th>
			<th>subtotal</th>
		   </thead>
		   <?php
	 	 			$sql2 = "SELECT order_detail_id, order_product_customer_id, full_name, order_product_product_code, order_product_product_batch_sale_id, SUM(qty) 'qty', sellprice FROM order_detail INNER JOIN customer ON order_product_customer_id = id INNER JOIN order_product ON order_product_customer_id = customer_id AND order_product_product_code = product_code AND order_product_product_batch_sale_id = product_batch_sale_id INNER JOIN product ON order_product_product_code = code WHERE order_product_customer_id=$id GROUP BY order_product_product_code";
			         
					$result2 = mysqli_query( $db, $sql2 );

					while($row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC)) {
					$customer_id = $row2["order_product_customer_id"];
					$noinv = $row2["order_detail_id"];
					$fullname = $row2["full_name"];
					$aqty = $row2['qty'];
					$sp = $row2['sellprice'];
					$st = $aqty*$sp;
					
					echo '
						<tr>
							<td>'.$row2["order_product_product_code"].'</td>
							<td>'.$row2["qty"].'</td>
		  					<td>'.number_format($sp, 0, ',', '.').'</th>
		  					<td>'.number_format($st, 0, ',', '.').'</th>
						</tr>';
					}

	 	 	 ?>
	     </table>
	 	</div>
	    </div>
  	   </div>
	  </div>
	 </div>
	</div>
</div>    

</body>
</html>