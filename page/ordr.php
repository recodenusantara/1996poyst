<?php
   include('config/connection.php');
?>
		<div class="content" id="content">
		        <div class="row">
		          <div class="col-md-12">
		            <div class="card demo-icons">
		              <div class="card-header">
		                <p class="card-category">Dashboard > Order</p>
		              </div>

		              <div class="card-body all-icons" >
		              	<div id="batch-wrapper">
		              		<section id="nav">
		              			<ul>
		              				<li>
		              					<a href="page/addorder.php">
		              						<i class="nc-icon nc-simple-add"></i>
		              						<p>Buat Orderan</p>
		              					</a>
		              				</li>
		              				<li>
		              					<a href="page/listorder.php">
		              						<i class="nc-icon nc-cart-simple"></i>
		              						<p>List Product Orderan</p>
		              					</a>
		              				</li>
		              				<li>
		              					<a href="page/epds.php">
		              						<i class="nc-icon nc-money-coins"></i>
		              						<p>Biaya Pengeluaran</p>
		              					</a>
		              				</li>
		              				<li>
		              					<a href="#print_inv" data-toggle="modal" data-target="#print_inv_modal">
		              						<i class="nc-icon nc-single-copy-04"></i>
		              						<p>Cetak Invoice</p>
		              					</a>
		              				</li>
		              				<li>
		              					<a href="page/pbyr.php">
		              						<i class="nc-icon nc-alert-circle-i"></i>
		              						<p>Pembayaran</p>
		              					</a>
		              				</li>
		              			</ul>
		              		</section>
		              	</div>
		              </div>
             </div>
            </div> 
		</div>



</div>