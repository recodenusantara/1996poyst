<?php
   include('../config/session.php');
   include('../config/connection.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png">
	  <link rel="icon" type="image/png" href="img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Order</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="../css/bootstrap.min.css" rel="stylesheet" />
  <link href="../css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="../img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="../dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="../product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="../customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li class="active">
		          	<a href="../order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="../user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include '../page/navbar.php' ?>  

		    <!-- dashboard list view -->
			<div class="content">
	<div class="row">
	 <div class="col-md-12">
	  <div class="card">
	   <div class="card-header">
	   		<div class="d-flex justify-content-between">
	   			<div>
					<h4 class="card-title"> Daftar Orderan</h4>
		 			<p class="card-category">Dashboard > Order > List Orderan</p>
		 		</div>
			</div>
	   </div>
	  	<div class="card-body">
	 	 <div class="table-responsive">
		  <table class="table">
		   <thead class=" text-primary">
			<th>
			 Kode Produk
			</th>
			<th>
			 Nama Produk
			</th>
			<th>
 			 Harga Modal
			</th>
			<th>
 			 Vendor
			</th>
			<th>
			 Quantity
			</th>
			<th>
			 
			</th>
		   </thead>
		  <tbody>
		  	<?php
		        $sql = "SELECT product_code, nama_product, buyprice, vendor, SUM(qty) 'qty' FROM order_product INNER JOIN product ON product_code = code GROUP BY product_code";
									         
				$result = mysqli_query( $db, $sql );

				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			?>
		  	<tr>				                    
		  	<td>
		  		<?php echo $row['product_code'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['nama_product'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['buyprice'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['vendor'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['qty'] ?>
		  	</td>
		  	</tr>
		  	<?php
				}
			?>
		  </tbody>
	     </table>
	    </div>
  	   </div>
	  </div>
	 </div>
	</div>
</div>    
	
	<?php include '../footer.php' ?>
	
	<!--   Core JS Files   -->
  	<script src="../js/core/jquery.min.js"></script>
  	<script src="../js/core/popper.min.js"></script>
  	<script src="../js/core/bootstrap.min.js"></script>
  	<script src="../js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="../js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="../js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="../js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

</body>
</html>