<?php
   include('config/connection.php');
?>
<div class="content">
	<div class="row">
	 <div class="col-md-12">
	  <div class="card">
	   <div class="card-header">
	   		<div class="d-flex justify-content-between">
	   			<div>
					<h4 class="card-title"> Daftar User</h4>
		 			<p class="card-category">Dashboard > User</p>
		 		</div>
		 		<div class="d-flex align-items-center">
		 			<button class="btn btn-primary btn-block text-uppercase" data-toggle="modal" data-target="#add_user_modal"><i class="nc-icon nc-simple-add"></i> Tambah User Baru</button>
				</div>
			</div>
	   </div>
	  	<div class="card-body">
	 	 <div class="table-responsive">
		  <table class="table">
		   <thead class=" text-primary">
			<th>
			 User Name
			</th>
			<th>
			 Nama  
			</th>
			<th>

			</th>
		   </thead>
		  <tbody>
		  	<?php
		        $sql = "SELECT * FROM user";
									         
				$result = mysqli_query( $db, $sql );

				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			?>
		  	<tr>		                    
		  	<td>
		  		<?php echo $row['username'] ?>
		  	</td>
		  	<td>
		  		<?php echo $row['nama_lengkap'] ?>
		  	</td>
		  	<td>
		  		<button class="btn btn-success openModal" data-toggle="modal" data-target="#view_user_modal" data-id="<?php echo $row['id'] ?>">
		  			<i class="nc-icon nc-ruler-pencil"></i>
		  		</button>
		  	</td>
		  	</tr>
		  	<?php
				}
			?>
		  </tbody>
	     </table>
	    </div>
  	   </div>
	  </div>
	 </div>
	</div>
</div>     	