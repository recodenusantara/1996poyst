<?php
   include('../config/session.php');
   include('../config/connection.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	  <link rel="apple-touch-icon" sizes="76x76" href="../img/logo.png">
	  <link rel="icon" type="image/png" href="../img/logo.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>1996Poyst - Product</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  
  <link href="../css/bootstrap.min.css" rel="stylesheet" />
  <link href="../css/1996poyst.css?v=2.0.0" rel="stylesheet" />
</head>
<body class=".main-panel">
	<div class="wrapper">
  
		<!-- side Navbar -->
		<div class="sidebar" data-color="white" data-active-color="danger">
			<!-- nama user terlogin -->
			<div class="logo">
        		<a href="#" class="simple-text logo-mini">
          			<div class="logo-image-small">
            			<img src="../img/logo.png">
          			</div>
        		</a>
        		<a href="#" class="simple-text logo-normal">
       				<?php echo $login_session; ?>
       			</a>
			</div>
			<!-- list menu -->
			<div class="sidebar-wrapper">
		        <ul class="nav">
		        	<!-- Dashboard -->
		          <li>
		            <a href="../dashboard.php">
		              <i class="nc-icon nc-shop"></i>
		              	<p>Dashboard</p>
		            </a>
		          </li>
		          	<!-- Product -->
		          <li>
		          	<a href="../product.php">
		          		<i class="nc-icon nc-diamond"></i>
		          			<p>Product</p>
		          	</a>
		          </li>
		          	<!-- Customer -->
		          <li>
		          	<a href="../customer.php">
		          		<i class="nc-icon nc-book-bookmark"></i>
		          			<p>Customer</p>
		          	</a>
		          </li>
		          	<!-- order -->
		          <li class="active">
		          	<a href="../order.php">
		          		<i class="nc-icon nc-cart-simple"></i>
		          			<p>Order</p>
		          	</a>
		          </li>
		          	<!-- user -->
		          <li>
		          	<a href="../user.php">
		          		<i class="nc-icon nc-badge"></i>
		          			<p>User</p>
		          	</a>
		          </li>
		      </ul>
		  </div>
		</div>

		<div class="main-panel">

			<!-- Navbar -->
		    <?php include '../page/navbar.php' ?>  

		    <!-- dashboard batch view -->
		<div class="content" id="content">
		     <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Pembayaran</h5>
                <p class="card-category">Dashboard > Order > Pembayaran</p>
              </div>
              <div class="card-body">
                <form action="upload.php" method="POST" enctype="multipart/form-data" >
                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Kode Customer</label>
                        <input id="kode_inv" name="kode_inv" type="text" class="form-control pencarian" value="">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                            Pilih File : <input class="form-control" type="file" name="image">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Keterangan</label>
                        <select name="keterangan" class="form-control">
                        <option value="Lunas">Lunas</option>
                        <option value="Belum Bayar">Belum Bayar</option>
                        </select>

                      </div>
                    </div>
                    
                  </div>

                  <div class="row">
                    <div class="col-md-3 pr-1">
                      <div class="form-group">
                        <label>Kode Batch</label>
                        <input name="kode_batch" name="kode_batch" type="number" class="form-control" value="">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="submit" name="upt_payment" class="btn btn-primary btn-round">Selesai</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
             	</div>

	<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
           <!-- Modal content-->
           <div class="modal-content">
              <div class="modal-header">
              	<center>  <span class="modal-title">Nomor Batch</span></center>
			          	<button type="button" class="close" data-dismiss="modal">×</button>
              </div>
              <div class="modal-body">
                 <table id="example" class="table table-bordered">
                    <thead>
                       <tr>
                          <th>No. Invoice</th>
                          <th>Nama Customer</th>
                       </tr>
                    </thead>
                    <tbody>
                    	<?php
					               $sql = "SELECT order_detail_id,order_product_customer_id,full_name FROM order_detail INNER JOIN customer ON order_product_customer_id = id GROUP BY id";
												         
							$result = mysqli_query( $db, $sql );

							while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
						echo '<tr id="data" onClick="masuk(this,'.$row['order_product_customer_id'].')" href="javascript:void(0)">
                          <td><a id="data" onClick="masuk(this,'.$row['order_product_customer_id'].')" href="javascript:void(0)">'.$row['order_detail_id'].'</a></td>
                          <td>'.$row['full_name'].'</td>
                       </tr>';
							}
						?>
                    </tbody>
                 </table>
              </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
           </div>
        </div>
     </div>

     <?php include '../footer.php' ?>

	<!--   Core JS Files   -->
  	<script src="../js/core/jquery.min.js"></script>
  	<script src="../js/core/popper.min.js"></script>
  	<script src="../js/core/bootstrap.min.js"></script>
  	<script src="../js/plugins/perfect-scrollbar.jquery.min.js"></script>

  	<!-- Chart JS -->
  	<script src="../js/plugins/chartjs.min.js"></script>
  	<!--  Notifications Plugin    -->
  	<script src="../js/plugins/bootstrap-notify.js"></script>
  	<!-- SFX -->
  	<script src="../js/1996poyst.min.js?v=2.0.0" type="text/javascript"></script>

</body>
</html>

<script>
    $('.openModalOrder').click(function(){
      var batchno = $(this).attr('data-id');

      $('#kode_batch').val(batchno);
      $('#kode_batch_hidden').val(batchno);

      $('#search_Batch').modal('hide');
	});

	$('.openModalCustomer').click(function(){
      var id = $(this).attr('data-id');

      $.ajax({
	         type: "POST",     // We want to use POST when we request our PHP file
	         url : "../cust.php",
	         data : { getname : id },    // passing an array to the PHP file with the param,  value you passed, this could just be a single value i.e. data: your_param
	         cache: false,       // disable the cache optional

	         // Success callback if the PHP executed  
	         success: function(data) {
	              // do somethig - i.e. update your modal with the returned data object
      			$('#id_customer').val(id);
      			$('#nama_customer').val(data);
		        $('#search_Customer').modal('hide');
	         }

	     });
	});

	$('.openModalProduct').click(function(){
      var kode_product = $(this).attr('data-id');

      $('#kode_product').val(kode_product);
      $('#kode_product_hidden').val(kode_product);

      $('#search_Product').modal('hide');
	});

	$(document).ready(function() {
  //focusin berfungsi ketika cursor berada di dalam textbox modal langsung aktif
  $(".pencarian").focusin(function() {
    $("#myModal").modal('show'); // ini fungsi untuk menampilkan modal
  });
  $('#example').DataTable(); // fungsi ini untuk memanggil datatable
});

// function in berfungsi untuk memindahkan data kolom yang di klik menuju text box
function masuk(txt, data) {
  document.getElementById('kode_inv').value = data; // ini berfungsi mengisi value  yang ber id textbox
  $("#myModal").modal('hide'); // ini berfungsi untuk menyembunyikan modal
}
	
</script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>